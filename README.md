# What's Happening

Run an [express](https://expressjs.com/) app server.

Compile JS, CSS, and index.html with [webpack](https://webpack.js.org/). Use appropriate webpack loaders for CSS.

Run webpack from express using [webpack-dev-middleware](https://github.com/webpack/webpack-dev-middleware#webpack-dev-middleware) so that you don't need webpack-dev-sever for HMR (hot module replacement).

Poll for changes from the client to the server. Download and apply updates. Use [webpack-hot-middleware](https://github.com/glenjamin/webpack-hot-middleware#installation--usage) on the server and the webpack built-in [HotModuleReplacementPlugin](https://webpack.js.org/plugins/hot-module-replacement-plugin/) in the client webpack config.

"devDependencies": {
  "babel": "^6.23.0",
  "babel-core": "^6.26.0",
  "babel-loader": "^7.1.2",
  "babel-preset-es2015": "^6.24.1",
  "babel-preset-react": "^6.24.1",
  "css-loader": "^0.28.9",
  "html-webpack-plugin": "^2.30.1",
  "pre-commit": "^1.2.2",
  "style-loader": "^0.20.1",
  "webpack": "^3.11.0",
  "webpack-dev-middleware": "^2.0.5",
  "webpack-dev-server": "^2.11.1",
  "webpack-hot-middleware": "^2.21.0"
},
"dependencies": {
  "express": "^4.16.2",
  "react": "^16.2.0",
  "react-dom": "^16.2.0"
},
"pre-commit": [ ]
