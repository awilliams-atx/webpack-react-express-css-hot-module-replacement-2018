import React from 'react'
import ReactDOM from 'react-dom'
// CSS Step 2: import your css in your JS files (check out index.js)
import myCss from './index.css'

document.addEventListener('DOMContentLoaded', () => {
  const root = document.getElementById('root')
  ReactDOM.render(<div>hello world</div>, root)
})

