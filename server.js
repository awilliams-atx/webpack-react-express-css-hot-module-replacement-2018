const express = require('express')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpack.config')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const webpackDevConfig = Object.assign(webpackConfig, {
  // Watch for changes from the client and download updates
  // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
  entry: [
    webpackConfig.entry,
    'webpack-hot-middleware/client',
  ],
  output: {
    path: '/',
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: (webpackConfig.plugins || []).concat([
    // Serve index.html in a weird but popular way
    // https://github.com/jantimon/html-webpack-plugin
    new HtmlWebpackPlugin({
      template: 'index.html',
    }),
    // Watch for changes from the client and download updates
    // https://github.com/glenjamin/webpack-hot-middleware
    new webpack.HotModuleReplacementPlugin(),
  ]),
})

const compiler = webpack(webpackDevConfig)

const app = express()

// Serve webpack files
// http://madole.github.io/blog/2015/08/26/setting-up-webpack-dev-middleware-in-your-express-application/
app.use(webpackDevMiddleware(compiler, {
  logLevel: 'debug',
  publicPath: webpackConfig.output.publicPath,
}))

// Watch for changes from the client and download updates
// http://madole.github.io/blog/2015/08/26/setting-up-webpack-dev-middleware-in-your-express-application/
app.use(webpackHotMiddleware(compiler))

app.listen(3000, () => {
  console.log('Listening on 3000')
})

