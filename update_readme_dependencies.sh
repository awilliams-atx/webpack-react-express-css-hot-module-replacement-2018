script_filename_1=$dtmp/.SCRIPT-update_readme_dependencies1
num_lines=`cat README.md | sed -n '1,/devDepend/p' | wc -l`
num_lines="$(( num_lines - 1 ))"
head -n $num_lines README.md > $script_filename_1

script_filename_2=$dtmp/.SCRIPT-update_readme_dependencies2
num_total_lines=`cat package.json | sed -n '/devDepend/,$p' | wc -l`
num_lines_minus_one="$(( num_total_lines - 1 ))"
cat package.json | sed -n '/devDepend/,$p' | sed -n '1,'$num_lines_minus_one'p' | sed 's/  //' > $script_filename_2

script_filename_3=$dtmp/.SCRIPT-update_readme_dependencies3
cat $script_filename_2 >> $script_filename_1

cat $script_filename_1 > README.md

